package com.ecci.groupie

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.ecci.groupie.data.RetrofitInstance
import com.ecci.groupie.data.model.Case
import com.ecci.groupie.data.model.Country
import com.ecci.groupie.databinding.ActivityCasesBinding
import com.ecci.groupie.databinding.ActivityMainBinding
import com.xwray.groupie.GroupieAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CasesActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityCasesBinding>(this, R.layout.activity_cases)

        val code = intent.getStringExtra("code")

        //Se puede manipular el binding
        binding.casesRecyclerView.layoutManager = LinearLayoutManager(this)

        val adapter = GroupieAdapter()
        binding.casesRecyclerView.adapter = adapter

        val activity = this

        RetrofitInstance().covidApi().getCases(code ?: "").enqueue(object :
            Callback<List<Case>> {
            override fun onResponse(call: Call<List<Case>>, response: Response<List<Case>>) {
                response.body()?.let { list ->
                    val casesItems = list.map { CaseItem(it) }
                    adapter.update(casesItems)
                } ?: run {
                    if (response.code() == 401) {
                        Toast.makeText(activity, "El servicio no tiene el token", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(activity, "El servicio ha fallado", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onFailure(call: Call<List<Case>>, t: Throwable) {
                Toast.makeText(activity, "El servicio ha fallado", Toast.LENGTH_SHORT).show()
            }

        })
    }
}