package com.ecci.groupie

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.ecci.groupie.data.RetrofitInstance
import com.ecci.groupie.data.model.Country
import com.ecci.groupie.databinding.ActivityMainBinding
import com.xwray.groupie.ExpandableGroup
import com.xwray.groupie.GroupieAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Se genera el binding
        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)

        //Se puede manipular el binding
        binding.countriesRecyclerView.layoutManager = LinearLayoutManager(this)


        val adapter = GroupieAdapter()
        binding.countriesRecyclerView.adapter = adapter

        val activity = this

        RetrofitInstance().covidApi().getCountries().enqueue(object :
            Callback<List<Country>> {
            override fun onResponse(call: Call<List<Country>>, response: Response<List<Country>>) {
                response.body()?.let { list ->
                    val countriesItem: List<CountryItem> = list.sortedBy { it.infectionRisk }.map { CountryItem(it) { code ->
                        val intent = Intent(activity, CasesActivity::class.java)
                        intent.putExtra("code", code)
                        activity.startActivity(intent)
                    }
                    }
                    adapter.update(countriesItem)
                } ?: run {
                    if (response.code() == 401) {
                        Toast.makeText(activity, "El servicio no tiene el token", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(activity, "El servicio ha fallado", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onFailure(call: Call<List<Country>>, t: Throwable) {
                Toast.makeText(activity, "El servicio ha fallado", Toast.LENGTH_SHORT).show()
            }

        })

    }
}