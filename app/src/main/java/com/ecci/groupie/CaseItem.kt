package com.ecci.groupie

import com.ecci.groupie.data.model.Case
import com.ecci.groupie.databinding.ItemCaseBinding
import com.xwray.groupie.databinding.BindableItem

class CaseItem(private val case: Case): BindableItem<ItemCaseBinding>() {

    override fun bind(viewBinding: ItemCaseBinding, position: Int) {
        viewBinding.dateTextView.text = case.date
        viewBinding.totalCasesTextView.text = "Total de casos: ${case.totalCases}"
        viewBinding.newCasesTextView.text = "Nuevos casos: ${case.newCases}"
        viewBinding.testsTextView.text = "Pruebas: ${case.totalTests}"
    }

    override fun getLayout(): Int {
        return R.layout.item_case
    }
}