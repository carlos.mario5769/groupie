package com.ecci.groupie.data.model

import com.google.gson.annotations.SerializedName

data class Country(

    @SerializedName("id")
    val id: String,

    @SerializedName("rank")
    val rank: Int,

    @SerializedName("Country")
    val name: String,

    @SerializedName("ThreeLetterSymbol")
    val code: String,

    @SerializedName("Infection_Risk")
    val infectionRisk: Double)