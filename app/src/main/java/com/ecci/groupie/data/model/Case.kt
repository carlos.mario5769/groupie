package com.ecci.groupie.data.model

import com.google.gson.annotations.SerializedName

data class Case(

    @SerializedName("date")
    val date: String,

    @SerializedName("total_cases")
    val totalCases: Int,

    @SerializedName("new_cases")
    val newCases: Int,

    @SerializedName("total_tests")
    val totalTests: Int

)