package com.ecci.groupie.data.api

import com.ecci.groupie.data.model.Case
import com.ecci.groupie.data.model.Country
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

interface CovidApi {

    @Headers("x-rapidapi-key: 8a6e281627mshf918ee517db9717p150194jsn325953bfdff0")
    @GET("npm-covid-data/southamerica")
    fun getCountries(): Call<List<Country>>


    @Headers("x-rapidapi-key: 8a6e281627mshf918ee517db9717p150194jsn325953bfdff0")
    @GET("covid-ovid-data/sixmonth/{code}")
    fun getCases(@Path("code") code: String): Call<List<Case>>

}