package com.ecci.groupie.data

import com.ecci.groupie.data.api.CovidApi
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitInstance {
    private fun getInstance(): Retrofit {
        val gson = GsonBuilder().create()
        val retrofit = Retrofit.Builder()
            .baseUrl("https://vaccovid-coronavirus-vaccine-and-treatment-tracker.p.rapidapi.com/api/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        return retrofit
    }

    fun covidApi(): CovidApi {
        return getInstance().create(CovidApi::class.java)
    }
}