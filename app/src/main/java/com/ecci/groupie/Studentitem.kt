package com.ecci.groupie

import com.ecci.groupie.databinding.ItemStudentBinding
import com.xwray.groupie.databinding.BindableItem

class Studentitem(val name: String, val phone: String): BindableItem<ItemStudentBinding>() {
    override fun bind(viewBinding: ItemStudentBinding, position: Int) {
        viewBinding.nameTextView.text = name
        viewBinding.phoneTextView.text = phone
    }

    override fun getLayout(): Int {
        return R.layout.item_student
    }
}