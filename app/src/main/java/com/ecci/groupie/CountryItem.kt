package com.ecci.groupie

import android.os.Build
import androidx.annotation.RequiresApi
import com.ecci.groupie.data.model.Country
import com.ecci.groupie.databinding.ItemCountryBinding
import com.xwray.groupie.ExpandableGroup
import com.xwray.groupie.ExpandableItem
import com.xwray.groupie.databinding.BindableItem

class CountryItem(private val country: Country, val callback: (String) -> Unit): BindableItem<ItemCountryBinding>() {

    override fun bind(viewBinding: ItemCountryBinding, position: Int) {
        viewBinding.nameTextView.text = country.name
        viewBinding.rankTextView.text = "Rango: ${country.rank}"
        viewBinding.infectionRiskTextView.text = "Infección: ${country.infectionRisk}"
        viewBinding.detailButton.setOnClickListener {
            callback(country.code)
        }
    }

    override fun getLayout(): Int {
        return R.layout.item_country
    }
}